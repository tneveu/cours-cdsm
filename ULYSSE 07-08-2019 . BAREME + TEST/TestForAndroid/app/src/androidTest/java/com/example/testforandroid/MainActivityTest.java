package com.example.testforandroid;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.rule.ActivityTestRule;
import android.widget.TextView;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public  ActivityTestRule<MainActivity> rule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void checkIfTextViewArePresent()
    {
        MainActivity activity = rule.getActivity();
        TextView tvUserName = activity.findViewById(R.id.textViewUserName);
        TextView tvUserEmail = activity.findViewById(R.id.textViewUserEmail);

        assertThat(tvUserName, notNullValue());
        assertThat(tvUserName, instanceOf(TextView.class));

        assertThat(tvUserEmail, notNullValue());
        assertThat(tvUserEmail, instanceOf(TextView.class));

    }

    @Test
    public void checkIfDataIsDisplayed()
    {
        MainActivity activity = rule.getActivity();
        TextView tvUserName = activity.findViewById(R.id.textViewUserName);
        TextView tvUserEmail = activity.findViewById(R.id.textViewUserEmail);

        assertThat(tvUserName.getText().toString(), is("Ulysse"));
        assertThat(tvUserEmail.getText().toString(), is("ulysseogez@gmail.com"));
    }
}
