package com.example.testforandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        User currentUser = new User("Ulysse", "ulysseogez@gmail.com");

        TextView tvUserName = findViewById(R.id.textViewUserName);
        TextView tvUserEmail = findViewById(R.id.textViewUserEmail);

        tvUserName.setText(currentUser.getUserName());
        tvUserEmail.setText(currentUser.getUserEmail());
    }
}
