package com.example.testforandroid;

public class User {

    private String userName;
    private String userEmail;

    public User(String userName, String userEmail) {
        this.userName = userName;
        this.userEmail = userEmail;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public static boolean validateUsername(String userName)
    {
        if ( !userName.isEmpty() && userName.length() > 1 && userName.length() < 255)
        {
            return true;
        }
        else {
            return false;
        }
    }
}
