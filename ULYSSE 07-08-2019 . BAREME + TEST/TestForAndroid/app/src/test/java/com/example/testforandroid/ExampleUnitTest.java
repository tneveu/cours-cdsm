package com.example.testforandroid;

import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void addition_isNotCorrect() { assertNotEquals(4, 2+3);}

    @Test
    public void User_CorrectName_ReturnTrue() {
        assertThat(User.validateUsername("Ulysse")).isTrue();
    }

    @Test
    public void User_InorrectName_ReturnTrue() {
        assertThat(User.validateUsername("L")).isFalse();
    }
}