package com.example.testforandroid;
import org.junit.Test;

import static com.google.common.truth.Truth.assertThat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ValidatorUnitTest {

    @Test
    public void CorrectEmailReturnTrue()
    {
        assertThat(DataValidator.validateEmail("ulysseogez@gmail.om")).isTrue();
    }

    @Test
    public void MissingAtReturnFalse()
    {
        assertThat(DataValidator.validateEmail("ulysseogezgmail.om")).isFalse();
    }

    @Test
    public void MissingDomainReturnFalse()
    {
        assertThat(DataValidator.validateEmail("ulysseogez@.om")).isFalse();
}

    @Test
    public void MissingNameReturnFalse()
    {
        assertThat(DataValidator.validateEmail("@.gmail.com")).isFalse();
    }

    @Test
    public void EmptyStringReturnFalse()
    {
        assertThat(DataValidator.validateEmail("")).isFalse();
    }

    @Test
    public void MissingExtensionReturnFalse()
    {
        assertThat(DataValidator.validateEmail("ulysseogez@gmail")).isFalse();
    }

}