package com.example.cdsm09.roomwordsample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewWordActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.android.wordlistsql.REPLY";

    private EditText mEditWordView;
    private EditText mEditAuthorView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_word);

        mEditWordView = findViewById(R.id.edit_word);
        mEditAuthorView = findViewById(R.id.edit_author);

        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Bundle extras = new Bundle();

                Intent replyIntent = new Intent();

                if (TextUtils.isEmpty(mEditWordView.getText() ) || TextUtils.isEmpty(mEditAuthorView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {

                    String author = mEditAuthorView.getText().toString();
                    extras.putString("INPUT_AUTHOR",author);

                    String word = mEditWordView.getText().toString();
                    extras.putString("INPUT_WORD",word);


                    replyIntent.putExtra("Bundle", extras);
                    setResult(RESULT_OK, replyIntent);

                    /*String author = mEditAuthorView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, author);

                    String word = mEditWordView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, word);*/



                }
                finish();
            }
        });
    }
}
