package com.example.cdsm09.roomwordsample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewAuthorActivity extends AppCompatActivity {

    public static final String EXTRA_REPLY = "com.example.android.wordlistsql.REPLY";

    private EditText mEditFirstnameView;
    private EditText mEditNameView;
    private EditText mEditDateNaissanceView;
    private EditText mEditEmailView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_author);

        mEditFirstnameView = findViewById(R.id.edit_firstname);
        mEditNameView = findViewById(R.id.edit_name);
        mEditDateNaissanceView = findViewById(R.id.edit_datenaissance);
        mEditEmailView = findViewById(R.id.edit_email);


        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Bundle extras_author = new Bundle();

                Intent replyIntent = new Intent();

                if (TextUtils.isEmpty(mEditFirstnameView.getText() ) || TextUtils.isEmpty(mEditNameView.getText()) || TextUtils.isEmpty(mEditDateNaissanceView.getText()) || TextUtils.isEmpty(mEditEmailView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {

                    String firstname = mEditFirstnameView.getText().toString();
                    extras_author.putString("INPUT_FIRSTNAME",firstname);

                    String name = mEditNameView.getText().toString();
                    extras_author.putString("INPUT_NAME",name);

                    String dateNaissance = mEditDateNaissanceView.getText().toString();
                    extras_author.putString("INPUT_DATE_NAISSANCE",dateNaissance);

                    String email = mEditEmailView.getText().toString();
                    extras_author.putString("INPUT_EMAIL",email);


                    replyIntent.putExtra("Bundle_author", extras_author);
                    setResult(106, replyIntent);

                    /*String author = mEditAuthorView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, author);

                    String word = mEditWordView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, word);*/



                }
                finish();
            }
        });
    }
}
