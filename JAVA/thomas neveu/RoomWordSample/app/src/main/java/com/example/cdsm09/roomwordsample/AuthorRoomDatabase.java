package com.example.cdsm09.roomwordsample;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.util.Date;

@Database(entities = {Author.class}, version = 1)
public abstract class AuthorRoomDatabase extends RoomDatabase {



    public abstract AuthorDao authorDao();

    private static volatile AuthorRoomDatabase INSTANCE;



    static AuthorRoomDatabase getDatabase(final Context context) {


        if (INSTANCE == null) {
            synchronized (AuthorRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            AuthorRoomDatabase.class, "author_database")
                            .addCallback(sRoomDatabaseCallback)

                            .build();
                }
            }
        }
        return INSTANCE;
    }


    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };
    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

        private final AuthorDao mDao;

        PopulateDbAsync(AuthorRoomDatabase db) {
            mDao = db.authorDao();
        }



        @Override
        protected Void doInBackground(final Void... params) {
            Author author = new Author("jean","jacque" , 6654L, "jen.jacques@gmail.com");
            mDao.insert(author);
            return null;
        }
    }

}

