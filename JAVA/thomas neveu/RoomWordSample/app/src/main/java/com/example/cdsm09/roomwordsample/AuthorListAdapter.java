package com.example.cdsm09.roomwordsample;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class AuthorListAdapter extends RecyclerView.Adapter<AuthorListAdapter.AuthorViewHolder> {

    class AuthorViewHolder extends RecyclerView.ViewHolder {
        private final TextView firstnameItemView;
        private final TextView nameItemView;
        private final TextView datenaissanceItemView;
        private final TextView emailItemView;


        private AuthorViewHolder(View itemView) {
            super(itemView);
            firstnameItemView = itemView.findViewById(R.id.firstnameView);
            nameItemView = itemView.findViewById(R.id.nameView);
            datenaissanceItemView = itemView.findViewById(R.id.datenaissanceView);
            emailItemView = itemView.findViewById(R.id.emailView);
        }
    }

    private final LayoutInflater mInflater;
    private List<Author> mAuthors; // Cached copy of words

    AuthorListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public AuthorViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View itemView = mInflater.inflate(R.layout.recyclerviewauthor_item, parent, false);

        return new AuthorViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AuthorViewHolder holder, int position) {
        if (mAuthors != null) {
            Author current = mAuthors.get(position);
            holder.firstnameItemView.setText(current.getFirstname());
            holder.nameItemView.setText(current.getName());
            holder.datenaissanceItemView.setText(current.getDateNaissance().toString());
            holder.emailItemView.setText(current.getEmail());
        } else {
            // Covers the case of data not being ready yet.
            holder.firstnameItemView.setText("No author");
        }
    }

    void setAuthors(List<Author> authors){
        mAuthors = authors;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mAuthors != null)
            return mAuthors.size();
        else return 0;
    }
}
