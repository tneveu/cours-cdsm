package com.example.cdsm09.roomwordsample;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
public interface AuthorDao {

    @Insert
    void insert(Author author);

    @Query("DELETE FROM author_table")
    void deleteAll();

    @Query("SELECT * from author_table ORDER BY id ASC")
    LiveData<List<Author>> getAllAuthors();
}
