package com.example.cdsm09.roomwordsample;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import java.util.List;

public class AuthorRepository {

    private AuthorDao mAuthorDao;
    private LiveData<List<Author>> mAllAuthors;

    AuthorRepository(Application application) {
        AuthorRoomDatabase db = AuthorRoomDatabase.getDatabase(application);
        mAuthorDao = db.authorDao();
        mAllAuthors = mAuthorDao.getAllAuthors();
    }

    LiveData<List<Author>> getAllAuthors() {
        return mAllAuthors;
    }


    public void insert (Author author) {
        new insertAsyncTask(mAuthorDao).execute(author);
    }

    private static class insertAsyncTask extends AsyncTask<Author, Void, Void> {

        private AuthorDao mAsyncTaskDao;

        insertAsyncTask(AuthorDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Author... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}

