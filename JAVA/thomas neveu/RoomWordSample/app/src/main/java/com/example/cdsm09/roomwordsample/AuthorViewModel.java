package com.example.cdsm09.roomwordsample;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;

import java.util.List;

public class AuthorViewModel extends AndroidViewModel {

    private AuthorRepository aRepository;

    private LiveData<List<Author>> mAllAuthors;

    public AuthorViewModel (Application application) {
        super(application);
        aRepository = new AuthorRepository(application);
        mAllAuthors = aRepository.getAllAuthors();
    }

    LiveData<List<Author>> getAllAuthors() { return mAllAuthors; }

    public void insert(Author author) { aRepository.insert(author); }
}

