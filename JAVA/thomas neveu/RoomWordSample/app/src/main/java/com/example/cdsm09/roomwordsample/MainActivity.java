package com.example.cdsm09.roomwordsample;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.List;


public class MainActivity extends AppCompatActivity {

    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;
    private WordViewModel mWordViewModel;


    public static final int NEW_AUTHOR_ACTIVITY_REQUEST_CODE = 1;
    private AuthorViewModel mAuthorViewModel;




    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Bundle extras = data.getBundleExtra("Bundle");
        Bundle extras_author = data.getBundleExtra("Bundle_author");

        super.onActivityResult(requestCode, resultCode, data);









        if (requestCode == NEW_AUTHOR_ACTIVITY_REQUEST_CODE && resultCode == 106) {

            String firstname = extras_author.getString("INPUT_FIRSTNAME");
            String name = extras_author.getString("INPUT_NAME");
            Long datenaissance = extras_author.getLong("INPUT_DATE_NAISSANCE");
            String email = extras_author.getString("INPUT_EMAIL");

            Author Oauthor = new Author(firstname, name , datenaissance, email);


            mAuthorViewModel.insert(Oauthor);
        }


        else if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String author = extras.getString("INPUT_AUTHOR");
            String w = extras.getString("INPUT_WORD");
            Word word = new Word(author, w);



            mWordViewModel.insert(word);
        } else {
            Toast.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {



        mWordViewModel = ViewModelProviders.of(this).get(WordViewModel.class);
        mAuthorViewModel = ViewModelProviders.of(this).get(AuthorViewModel.class);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab_author = (FloatingActionButton) findViewById(R.id.fab_author);
        fab_author.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewAuthorActivity.class);
                startActivityForResult(intent, NEW_AUTHOR_ACTIVITY_REQUEST_CODE);
            }
        });

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, NewWordActivity.class);
                startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE);
            }
        });

        RecyclerView recyclerViewAuthor = findViewById(R.id.recyclerviewauthor);
        final AuthorListAdapter adapterAuthor = new AuthorListAdapter(this);
        recyclerViewAuthor.setAdapter(adapterAuthor);
        recyclerViewAuthor.setLayoutManager(new LinearLayoutManager(this));
        mAuthorViewModel.getAllAuthors().observe(this, new Observer<List<Author>>() {
            @Override
            public void onChanged(@Nullable final List<Author> authors) {
                // Update the cached copy of the words in the adapter.
                adapterAuthor.setAuthors(authors);
            }
        });

        RecyclerView recyclerView = findViewById(R.id.recyclerview);
        final WordListAdapter adapter = new WordListAdapter(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        mWordViewModel.getAllWords().observe(this, new Observer<List<Word>>() {
            @Override
            public void onChanged(@Nullable final List<Word> words) {
                // Update the cached copy of the words in the adapter.
                adapter.setWords(words);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
