package com.example.cdsm09.roomwordsample;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity(tableName = "author_table")
public class Author {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "firstname")
    private String firstname;


    @NonNull
    @ColumnInfo(name = "name")
    private String name;

    @NonNull
    @ColumnInfo(name = "dateNaissance")
    private Long dateNaissance;

    @NonNull
    @ColumnInfo(name = "email")
    private String email;



    public Author(@NonNull String firstname,@NonNull String name,@NonNull Long dateNaissance,@NonNull String email) {
        this.firstname = firstname;
        this.name = name;
        this.dateNaissance = dateNaissance;
        this.email = email;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @NonNull
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(@NonNull String firstname) {
        this.firstname = firstname;
    }

    @NonNull
    public String getName() {
        return name;
    }

    public void setName(@NonNull String name) {
        this.name = name;
    }

    @NonNull
    public Long getDateNaissance() {
        return dateNaissance;
    }

    public void setDateNaissance(@NonNull Long dateNaissance) {
        this.dateNaissance = dateNaissance;
    }

    @NonNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NonNull String email) {
        this.email = email;
    }

    public int getAge() {
        return 6;
    }
}
