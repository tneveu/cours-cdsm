package com.example.cdsm09.libraryapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.util.List;



public class MainActivity extends AppCompatActivity {

    LinearLayout layoutBooks;
    LinearLayout bookrow;

    private List<Book> listBooks;
    BibliDatabase bdb;
//    public TextView titreBook1;
//    public TextView auteurBook1;
//    public TextView resumeBook1;


    @Override
    protected void onStart() {
        super.onStart();
        new GetBookTask().execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        layoutBooks = findViewById(R.id.layoutBooks);


        bdb = BibliDatabase.getDatabase(this);


        FloatingActionButton add = findViewById(R.id.floatingActionButton);
        add.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getBaseContext(), AddBookActivity.class);
                startActivity(intent);
            }
        });

//        Button updateBtn = findViewById(R.id.btnUpdate);
//        updateBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new UpdateBootTask().execute();
//
//            }
//        });
//
//        Button insertBtn = findViewById(R.id.btnInsert);
//        insertBtn.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                new InsertBookTask().execute();
//
//            }
//        });




    }

    private class GetBookTask extends AsyncTask<Void, Void, Void>{


        @Override
        protected Void doInBackground(Void... voids) {
            listBooks = bdb.bookDao().getAll();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            layoutBooks.removeAllViews();
            for (Book book : listBooks){

                final long id = book.Id;
                TextView tvBookTitle = new TextView(MainActivity.this);
                Button editBookTitle = new Button(MainActivity.this);
                Button deleteBookTitle = new Button(MainActivity.this);

                editBookTitle.setText("Modifier");
                deleteBookTitle.setText("Supprimer");

                tvBookTitle.setText(book.Titre);
                editBookTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), EditActivity.class);
                        intent.putExtra("CONST_ID", id);
                        startActivity(intent);
                    }
                });

                deleteBookTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getBaseContext(), DeleteActivity.class);
                        intent.putExtra("CONST_ID", id);
                        startActivity(intent);
                    }
                });
                tvBookTitle.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT));
                layoutBooks.addView(tvBookTitle);
                layoutBooks.addView(editBookTitle);
                layoutBooks.addView(deleteBookTitle);
            }
        }
    }
}
