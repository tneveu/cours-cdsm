package com.example.cdsm09.libraryapp;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class AddBookActivity extends AppCompatActivity {

    EditText editTitle;
    EditText editAuthor;
    EditText editResume;

    BibliDatabase db;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_book);
        db = BibliDatabase.getDatabase(this);
        editTitle = findViewById(R.id.title);
        editAuthor = findViewById(R.id.author);
        editResume = findViewById(R.id.resume);

        Button insertBtn = findViewById(R.id.insert);
        insertBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new InsertBookTask().execute();
            }
        });
    }

    private class InsertBookTask extends AsyncTask<Void, Void, Void > {

        @Override
        protected Void doInBackground(Void... voids){
            super.onPreExecute();

            Book book0 = new Book();


            book0.Auteur = editTitle.getText().toString();
            book0.Titre = editAuthor.getText().toString();
            book0.Resume = editResume.getText().toString();

            db.bookDao().insertBook(book0);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(AddBookActivity.this ,"Ajouté avec succès", Toast.LENGTH_LONG).show();
            finish();

        }
    }
}
