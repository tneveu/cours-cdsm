package com.example.cdsm09.libraryapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DeleteActivity extends AppCompatActivity {

    BibliDatabase bdb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete);
        bdb = BibliDatabase.getDatabase(this);
        new GetTask().execute();
        FloatingActionButton deleteBtn = findViewById(R.id.floatingActionButton2);
        deleteBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DeleteTask().execute();
            }
        });


    }

    private class GetTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            Intent intent = getIntent();
            if (intent.getExtras() != null ){
                long id = intent.getExtras().getLong("CONST_ID");
                Book book = bdb.bookDao().findById(id);

                TextView titre = findViewById(R.id.readTitre);
                TextView auteur = findViewById(R.id.readAuteur);
                TextView resume = findViewById(R.id.readResume);

                titre.setText(book.Titre);
                auteur.setText(book.Auteur);
                resume.setText(book.Resume);
            }

            return null;
        }
    }

    private class DeleteTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            Intent intent = getIntent();
            if (intent.getExtras() != null ){
                long id = intent.getExtras().getLong("CONST_ID");
                Book book = bdb.bookDao().findById(id);




                bdb.bookDao().deleteBook(book);
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(DeleteActivity.this, "Le film a été supprimé", Toast.LENGTH_LONG);
            finish();
        }
    }
}
