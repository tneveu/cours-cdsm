package com.example.cdsm09.libraryapp;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class EditActivity extends AppCompatActivity {

    BibliDatabase bdb;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit);



        bdb = BibliDatabase.getDatabase(this);
        new GetTask().execute();

        Button updateBtn = findViewById(R.id.btnUpdate);
        updateBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new UpdateTask().execute();
            }
        });

    }

    private class GetTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {
            Intent intent = getIntent();
            if (intent.getExtras() != null ){
                long id = intent.getExtras().getLong("CONST_ID");
                Book book = bdb.bookDao().findById(id);

                TextView titre = findViewById(R.id.editTitre);
                TextView auteur = findViewById(R.id.editAuteur);
                TextView resume = findViewById(R.id.editResume);

                titre.setText(book.Titre);
                auteur.setText(book.Auteur);
                resume.setText(book.Resume);
            }
            return null;
        }


    }

    private class UpdateTask extends AsyncTask<Void, Void, Void>{

        @Override
        protected Void doInBackground(Void... voids) {

            Intent intent = getIntent();
            if (intent.getExtras() != null ){
                long id = intent.getExtras().getLong("CONST_ID");
                Book book = bdb.bookDao().findById(id);

                TextView titre = findViewById(R.id.editTitre);
                TextView auteur = findViewById(R.id.editAuteur);
                TextView resume = findViewById(R.id.editResume);

                book.Titre = titre.getText().toString();
                book.Auteur = auteur.getText().toString();
                book.Resume = resume.getText().toString();


                bdb.bookDao().updateBook(book);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            Toast.makeText(EditActivity.this, "Objet bien modifié", Toast.LENGTH_LONG);
            finish();
        }
    }







}
