package com.example.cdsm09.libraryapp;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import java.util.Date;

@Entity
public class Book {

    @PrimaryKey(autoGenerate = true)
    public long Id;

    @NonNull
    public String Titre;

    @NonNull
    public String Auteur;


    @NonNull
    public String Resume;


}
