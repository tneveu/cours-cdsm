package com.example.cdsm09.login;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;


public class IndexActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_index);

        ImageView image = (ImageView) findViewById(R.id.imageView);

        String myUri = getIntent().getStringExtra("personPhoto");


        TextView displayTextView = (TextView) findViewById(R.id.personName);
        String name = getIntent().getStringExtra("personName");

        TextView displayTextView2 = (TextView) findViewById(R.id.personGivenName);
        String personGivenName = getIntent().getStringExtra("personGivenName");

        displayTextView.setText(name);
        displayTextView2.setText(personGivenName);

        new DownloadImageTask((ImageView) findViewById(R.id.imageView)).execute(myUri);




    }
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new java.net.URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
