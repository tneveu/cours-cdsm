package com.example.cdsm09.mytestapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class DeuxiemeActivity extends AppCompatActivity {

    private Model model = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deuxieme);



        TextView txtValeur1 = findViewById(R.id.textValeur1);
        TextView txtValeur2 = findViewById(R.id.textValeur2);

        Bundle extras = getIntent().getExtras();


        if (extras != null){

            model = extras.getParcelable(MainActivity.KEY_MODEL);
            if (model != null){
                txtValeur1.setText(String.valueOf(model.getValeur1()));
                txtValeur2.setText(model.getValeur2());



            }



//            /*Premiere valeur int transformé en String avec ValueOf*/
//            txtValeur1.setText(String.valueOf(extras.getInt(MainActivity.KEY_VALEUR1, 0)));
//            txtValeur2.setText(extras.getString(MainActivity.KEY_VALEUR2,"poulet"));
        }
        Button btnAddTen = findViewById(R.id.btnAddTen);
        btnAddTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (model != null){
                    model.setValeur1(model.getValeur1() + 10 );
                    Intent intent = new Intent();
                    intent.putExtra(MainActivity.KEY_MODEL, model);

                    setResult(MainActivity.RESULT_SUCCESS_AGE_CHANGED, intent);
                }
                else{
                    setResult(MainActivity.RESULT_SUCCESS_AGE_NOT_CHANGED);
                }
                finish();
            }

        });

        Button btnCancel = findViewById(R.id.btnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(MainActivity.RESULT_CANCEL);
                finish();
            }

        });


    }
}
