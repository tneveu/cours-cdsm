package com.example.cdsm09.mytestapp;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    static final String KEY_MODEL = "key_model";
    static final int RESULT_SUCCESS_AGE_CHANGED = 100;
    static final int RESULT_SUCCESS_AGE_NOT_CHANGED = 101;
    static final int RESULT_CANCEL = 102;

    static final int ACTIVITY_DEUXIEME = 1000;


//    static final String KEY_VALEUR1 = "key valeur1";
//    static final String KEY_VALEUR2 = "key valeur2";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnNewAct = findViewById(R.id.btnNewActivity);
        btnNewAct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Model variableModel = new Model();

                variableModel.setValeur1(14);
                variableModel.setValeur2("HEYYYYYYYYYYYYY");

//                int valeur1 = 10;
//                String valeur2 = "Salut les amis";

                Intent intent = new Intent(MainActivity.this ,DeuxiemeActivity.class);
//                intent.putExtra(KEY_VALEUR1, valeur1);
                intent.putExtra(KEY_MODEL, variableModel);

                startActivityForResult(intent, ACTIVITY_DEUXIEME);
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ACTIVITY_DEUXIEME){
            switch (resultCode){
                case (RESULT_SUCCESS_AGE_CHANGED):{
                    if (data != null){
                        Model model = data.getParcelableExtra(KEY_MODEL);
                        TextView txtValeur1 = findViewById(R.id.modelValeur1);
                        TextView txtValeur2 = findViewById(R.id.modelValeur2);

                        txtValeur1.setText(String.valueOf(model.getValeur1()));
                        txtValeur2.setText(model.getValeur2());
                    }
                    break;
                }
                case (RESULT_SUCCESS_AGE_NOT_CHANGED) : {
                    Toast.makeText(this ,"Il n'ya pas eu de changment (model null dans la deuxieme activité)", Toast.LENGTH_SHORT).show();
                    break;
                }
                case (RESULT_CANCEL) : {
                    Toast.makeText(this,"Result cancel", Toast.LENGTH_SHORT).show();
                    break;
                }
            }
        }
    }
}
