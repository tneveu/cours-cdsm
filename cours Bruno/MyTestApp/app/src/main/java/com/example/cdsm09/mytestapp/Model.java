package com.example.cdsm09.mytestapp;

import android.os.Parcel;
import android.os.Parcelable;

public class Model implements Parcelable {

    private int valeur1 = 0;
    private String valeur2 = "";

    protected Model(Parcel in) {
        valeur1 = in.readInt();
        valeur2 = in.readString();
    }

    public Model() {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(valeur1);
        dest.writeString(valeur2);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Model> CREATOR = new Creator<Model>() {
        @Override
        public Model createFromParcel(Parcel in) {
            return new Model(in);
        }

        @Override
        public Model[] newArray(int size) {
            return new Model[size];
        }
    };

    public int getValeur1() {
        return valeur1;
    }

    public void setValeur1(int valeur1) {
        this.valeur1 = valeur1;
    }

    public String getValeur2() {
        return valeur2;
    }

    public void setValeur2(String valeur2) {
        this.valeur2 = valeur2;
    }
}
