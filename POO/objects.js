// JavaScript source code

/*
 * Creer une classe TimeManager avecles attributs
 * heures, minutes, secondes
 * 
 * les fonctions transformeSecondesEnHeuresEtMinutes
 * et transformeHeuresEtMinutesEnSecondes
 * 
 * et les fonctions getTempsEnObject et getTempsEnSecondes
 * 
 */
function TimeManager(temps) {

    this.transformeSecondesEnHeuresEtMinutes =
        function (secondes) {
            var _heures = Math.floor(secondes / 3600);
            var _minutes = Math.floor((secondes - _heures * 3600) / 60);
            var _secondes = Math.floor((secondes - (_heures * 3600) - (_minutes * 60)));

            return { heures: _heures, minutes: _minutes, secondes: _secondes }
        };

    this.transformeHeuresEtMinutesEnSecondes =
        function (timeObject) {
            return timeObject.heures * 3600
                + timeObject.minutes * 60
                + timeObject.secondes;
        };

    this.getTempsEnObject = function () {
        return {
            heures: this._heures,
            minutes: this._minutes,
            secondes: this._secondes
        }
    }

    this.getTempsEnSecondes = function () {
        return this.transformeHeuresEtMinutesEnSecondes(this.getTempsEnObject())
    }


    if (typeof temps == "object") {
        this._heures = temps.heures;
        this._minutes = temps.minutes;
        this._secondes = temps.secondes;
    }

    else if (typeof temps == "number") {
        var t = this.transformeSecondesEnHeuresEtMinutes(temps);
        this._heures = t.heures;
        this._minutes = t.minutes;
        this._secondes = t.secondes;
    }
}