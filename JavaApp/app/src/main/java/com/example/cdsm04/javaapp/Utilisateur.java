package com.example.cdsm04.javaapp;

public class Utilisateur {

    public String TAG = "Test.user";

    private String userName;
    private String userEmail;

    public Utilisateur(String userName, String userEmail) {
        this.userName = userName;
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }
}
