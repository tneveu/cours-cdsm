package com.example.cdsm04.javaapp;

import android.provider.BaseColumns;

public final class UserReaderContract {
    // To prevent someone from accidentally instantiating the contract class,
    // make the constructor private.
    private UserReaderContract() {}

    /* Inner class that defines the table contents */
    public static class UserEntry implements BaseColumns {
        public static final String TABLE_NAME = "user";
        public static final String COLUMN_NAME_NOM = "nom";
        public static final String COLUMN_NAME_EMAIL = "email";
    }
    public static final String SQL_CREATE_ENTRIES =
            "CREATE TABLE " + UserEntry.TABLE_NAME + " (" +
                    UserEntry._ID + " INTEGER PRIMARY KEY," +
                    UserEntry.COLUMN_NAME_NOM + " TEXT," +
                    UserEntry.COLUMN_NAME_EMAIL + " TEXT)";

    public static final String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + UserEntry.TABLE_NAME;


}
