package com.example.cdsm04.javaapp;

import android.util.Log;

public class Classes {
    private String nomClasse;


    public Classes(String _nomClasse) {
        this.setNomClasse(_nomClasse);
    }

    public String getNomClasse() {
        return nomClasse;
    }

    public void setNomClasse(String _nomClasse) {
        if(!_nomClasse.equals("pretre")&&!_nomClasse.equals("guerrier")&&!_nomClasse.equals("voleur")&&!_nomClasse.equals("mage"))
            Log.i("Test", "Error");
        this.nomClasse = _nomClasse;
    }
}
