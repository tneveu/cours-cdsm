package com.example.cdsm04.javaapp;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;




public class UserCreationActivity extends AppCompatActivity {
    UserReaderDbHelper mDbHelper = new UserReaderDbHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_creation);

    Button boutonVersPerso = (Button) findViewById(R.id.creaPerso);
        final EditText userNameEditText = (EditText)findViewById(R.id.user_name_input);
        final EditText userEmailEditText = (EditText)findViewById(R.id.user_email_input);
        boutonVersPerso.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View view){
            Utilisateur user = new Utilisateur(userNameEditText.getText().toString(), userEmailEditText.getText().toString());

            SQLiteDatabase db = mDbHelper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(UserReaderContract.UserEntry.COLUMN_NAME_NOM, user.getUserName());
            values.put(UserReaderContract.UserEntry.COLUMN_NAME_EMAIL, user.getUserEmail());
            long newRowId = db.insert(UserReaderContract.UserEntry.TABLE_NAME, null, values);

            Log.i("Test.creationUser","New user : "+ user.getUserName()+" email :" +user.getUserEmail()+ "import "+newRowId);

        }
    });
    }

}
