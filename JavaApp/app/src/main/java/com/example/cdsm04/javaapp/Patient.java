package com.example.cdsm04.javaapp;

public class Patient {
    public String maladie;
    public String mutuel;

    public String getMaladie() {
        return maladie;
    }

    public void setMaladie(String maladie) {
        this.maladie = maladie;
    }

    public String getMutuel() {
        return mutuel;
    }

    public void setMutuel(String mutuel) {
        this.mutuel = mutuel;
    }
}
