package com.example.cdsm04.javaapp;

import java.util.HashMap;
import java.util.Map;

public class Personnage {

    private String nomJoueur;
    private String nomPersonnage;
    private int experience;
    private Race racePersonnage;
    private Classes classe;
    private Alignement alignement;
    public Map<String, Caracteristique> caracteristiqueMap;

    public Personnage(String nomJoueur, String nomPersonnage, int experience) {
        this.nomJoueur = nomJoueur;
        this.nomPersonnage = nomPersonnage;
        this.experience = 0;
        caracteristiqueMap = new HashMap<>();
        Caracteristique force = new Caracteristique("force", 0);
        Caracteristique dexterite = new Caracteristique("dexterite", 0);
        Caracteristique constitution = new Caracteristique("constitution", 0);
        Caracteristique intelligence = new Caracteristique("intelligence", 0);
        Caracteristique sagesse = new Caracteristique("sagesse", 0);
        Caracteristique charisme = new Caracteristique("charisme", 0);

        caracteristiqueMap.put("force", force);
        caracteristiqueMap.put("dexterite", dexterite);
        caracteristiqueMap.put("constitution", constitution);
        caracteristiqueMap.put("intelligence", intelligence);
        caracteristiqueMap.put("sagesse", sagesse);
        caracteristiqueMap.put("charisme", charisme);

    }

    public String getNomJoueur() {
        return nomJoueur;
    }

    public void setNomJoueur(String _nomJoueur) {
        this.nomJoueur = _nomJoueur;
    }

    public String getNomPersonnage() {
        return nomPersonnage;
    }

    public void setNomPersonnage(String _nomPersonnage) {
        this.nomPersonnage = _nomPersonnage;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int _experience) {
        this.experience = _experience;
    }

    public Race getRacePersonnage() {
        return racePersonnage;
    }

    public void setRacePersonnage(Race racePersonnage) {
        this.racePersonnage = racePersonnage;
    }

    public Classes getClasse() {
        return classe;
    }

    public void setClasse(Classes classe) {
        this.classe = classe;
    }

    public Map<String, Caracteristique> getCaracteristiqueMap() {
        return caracteristiqueMap;
    }

    public void setCaracteristiqueMap(Map<String, Caracteristique> caracteristiqueMap) {
        this.caracteristiqueMap = caracteristiqueMap;
    }

    public Alignement getAlignement() {
        return alignement;
    }

    public void setAlignement(Alignement alignement) {
        this.alignement = alignement;
    }

    public String getAlignementComplet() {
        return alignement.getAlignementComplet();
    }
}
