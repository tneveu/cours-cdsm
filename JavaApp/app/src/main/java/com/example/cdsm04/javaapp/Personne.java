package com.example.cdsm04.javaapp;

import android.util.Log;

public class Personne {

    private String _prenom;
    private String _nom;
    private int _age;
    private String _location;
    private Adresse _adresse;
    private int[] nbrFavori = {0, 2, 3, 4, 5};

    public Personne() {
        _prenom = "john";
        _nom = "Doe";
        _age = 24;
    }

    public Personne(String nom, String prenom, int age) {
        _prenom = prenom;
        _nom = nom;
        _age = age;
    }

    public String getAdresse() {
        return _adresse.getAdresseComplete();
    }

    public Adresse getAdresseObject() {
        return _adresse;
    }

    public String get_prenom() {
        return _prenom;
    }

    public void set_prenom(String _prenom) {
        this._prenom = _prenom;
    }

    public String get_nom() {
        return _nom;
    }

    public void set_nom(String _nom) {
        this._nom = _nom;
    }

    public int getAge() {
        return _age;
    }

    public void setAge(int _years) {
        this._age = _age;
    }

    public String getIdentity() {
        return this._prenom + " " + this._nom;
    }

    public String get_location() {
        return _location;
    }

    public void set_location(String _location) {
        this._location = _location;
    }

    public String getNombresFavoris() {
        String phrases = "Mes nombres favoris sont:";
        for (int i = 0; i < nbrFavori.length; i++) {
            if (i < nbrFavori.length - 1) {
                phrases += nbrFavori[i] + ", ";
            } else {
                phrases += nbrFavori[i] + ".";
            }
        }
        return phrases;
    }

    public int sommesNombresFavoris() {
        int somme = 0;
        for (int nombre : nbrFavori) {
            somme += nombre;
        }
        return somme;
    }
}
