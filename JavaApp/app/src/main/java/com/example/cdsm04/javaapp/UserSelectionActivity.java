package com.example.cdsm04.javaapp;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.provider.BaseColumns;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class UserSelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_selection);

        final EditText userNameEditText = (EditText)findViewById(R.id.user_name_input);

        UserReaderDbHelper mDbHelper = new UserReaderDbHelper(this);


        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // Define a projection that specifies which columns from the database
// you will actually use after this query.
        String[] projection = {
                BaseColumns._ID,
                UserReaderContract.UserEntry.COLUMN_NAME_NOM,
                UserReaderContract.UserEntry.COLUMN_NAME_EMAIL
        };

        // Filter results WHERE "title" = 'My Title'
        String selection = UserReaderContract.UserEntry.COLUMN_NAME_NOM + " = ?";
        String[] selectionArgs = { "Bob" };

        // How you want the results sorted in the resulting Cursor
        String sortOrder = UserReaderContract.UserEntry.COLUMN_NAME_EMAIL + " DESC";

        Cursor cursor = db.query(
                UserReaderContract.UserEntry.TABLE_NAME,   // The table to query
                projection,             // The array of columns to return (pass null to get all)
                null,              // The columns for the WHERE clause
                null,          // The values for the WHERE clause
                null,                   // don't group the rows
                null,                   // don't filter by row groups
                sortOrder               // The sort order
        );

        List itemIds = new ArrayList<>();
        while(cursor.moveToNext()) {
            long itemId = cursor.getLong(
                    cursor.getColumnIndexOrThrow(UserReaderContract.UserEntry._ID));
            itemIds.add(itemId);
            Log.i("Test.lectureUser", "tamere le SQL "+itemId);
        }


        cursor.close();

        Button boutonVersPerso = (Button) findViewById(R.id.button7);
        boutonVersPerso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toActivity = new Intent(getBaseContext(),UserCreationActivity.class);
                startActivity(toActivity);

            }
        });





    }


}
