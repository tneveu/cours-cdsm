package com.example.cdsm04.javaapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Personne p = new Personne("HERMITE", "Anthony", 26);

        Adresse adresse = new Adresse(5, "bis", "avenue de fribourg", 68000, "colmar");

        String nom_personne = p.get_nom();
        String prenom_personne = p.get_prenom();
        String adresse_personne = "avenue de fribourg - COLMAR";
        int age_personne = p.getAge();

        Race human = new Race("human");

        Personnage zhartas = new Personnage("Anthony", "Zhartas", 0);
        zhartas.setRacePersonnage(human);
        Classes classePersonnage = new Classes("voleur");
        zhartas.setClasse(classePersonnage);

        Alignement alignement = new Alignement("chaotique", "Neutre");

        zhartas.setAlignement(alignement);

        zhartas.caracteristiqueMap.get("force").setValeurCaracteristique(12);
        zhartas.caracteristiqueMap.get("dexterite").setValeurCaracteristique(13);
        zhartas.caracteristiqueMap.get("constitution").setValeurCaracteristique(14);
        zhartas.caracteristiqueMap.get("sagesse").setValeurCaracteristique(18);
        zhartas.caracteristiqueMap.get("charisme").setValeurCaracteristique(13);



        final TextView joueurTextView = (TextView) findViewById(R.id.textView_Personnage_Joueur);
        final TextView nomTextView = (TextView) findViewById(R.id.textView_Personnage_Pseudo);
        final TextView xpTextView = (TextView) findViewById(R.id.textView_Personnage_Xp);
        final TextView raceTextView = (TextView) findViewById(R.id.textView_Personnage_Race);
        final TextView dexteriteTextView = (TextView) findViewById(R.id.textView_Personnage_Dexterite);



        joueurTextView.setText(String.valueOf(zhartas.getNomJoueur()));
        nomTextView.setText(String.valueOf(zhartas.getNomJoueur()));
        xpTextView.setText(String.valueOf(zhartas.getExperience()));
        raceTextView.setText(String.valueOf(zhartas.getRacePersonnage().getNomRace()));
        dexteriteTextView.setText(String.valueOf(zhartas.caracteristiqueMap.get("dexterite").getModificateurCaracteristique()));


    }
}
