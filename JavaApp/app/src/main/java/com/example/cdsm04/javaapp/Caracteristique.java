package com.example.cdsm04.javaapp;

import android.util.Log;

public class Caracteristique {

    private String caracteristique;
    private int valeurCaracteristique;

    public Caracteristique(String _caracteristique, int _valeurCaracteristique) {
        this.setCaracteristique(_caracteristique);
        this.valeurCaracteristique = _valeurCaracteristique;
    }

    public String getCaracteristique() {
        return caracteristique;
    }

    public void setCaracteristique(String _caracteristique) {
        if (!_caracteristique.equals("force") && !_caracteristique.equals("dexterite") && !_caracteristique.equals("constitution") && !_caracteristique.equals("sagesse") && !_caracteristique.equals("charisme")) {
            Log.i("Test", "caractéristique inconnue");
        }
        this.caracteristique = _caracteristique;
    }

    public int getValeurCaracteristique() {
        return valeurCaracteristique;
    }

    public void setValeurCaracteristique(int valeurCaracteristique) {
        this.valeurCaracteristique = valeurCaracteristique;
    }

    public int getModificateurCaracteristique() {
        return (int) (Math.floor(this.getValeurCaracteristique() - 10 / 2));
    }

}
