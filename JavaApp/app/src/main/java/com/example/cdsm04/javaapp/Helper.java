package com.example.cdsm04.javaapp;

import android.util.Log;

public final class Helper {

    static String TAG = "Test.Helper";

    static String getDefaultPrenom() {
        return "Kevin";
    }

    static String getDefaultNom() {
        return "Martin";
    }

    static void logPersonne(Personne p) {
        Log.i(TAG, "hey," + p.getIdentity() + " age " + p.getAge() + " nbr " + p.getNombresFavoris() + " sum " + p.sommesNombresFavoris());
    }

    static void afficheSommesNombresFavoris(Personne p) {
        Log.i(TAG, "afficheSommesNombresFavoris" + p.sommesNombresFavoris());
    }
}
