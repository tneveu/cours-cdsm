package com.example.cdsm04.javaapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class HomeScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        Button boutonActivity = (Button) findViewById(R.id.button);
        boutonActivity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toActivity = new Intent(getBaseContext(),MainActivity.class);
                startActivity(toActivity);
            }
        });


        Button boutonSelection = (Button) findViewById(R.id.button6);
        boutonSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent toActivity = new Intent(getBaseContext(),UserSelectionActivity.class);
                startActivity(toActivity);
            }
        });
    }


}
