package com.example.cdsm04.javaapp;

import android.util.Log;

public class Race {
    private String nomRace;

    public Race(String _nomRace) {
        this.setNomRace(_nomRace);
    }

    public String getNomRace() {
        return nomRace;
    }

    public void setNomRace(String _nomRace) {
        if (!_nomRace.equals("Nain") && !_nomRace.equals("Halfling") && !_nomRace.equals("Humain"))
            Log.i("Test", "Error");
        this.nomRace = _nomRace;
    }
}
