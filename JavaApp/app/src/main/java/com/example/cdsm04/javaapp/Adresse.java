package com.example.cdsm04.javaapp;

public class Adresse {
    public int _numeroRue;
    public String _type;
    public String _ville;
    public int _cp;
    public String _rue;

    public Adresse(int _numeroRue, String _type, String _ville, int _cp, String _rue) {
        this._numeroRue = _numeroRue;
        this._type = _type;
        this._ville = _ville;
        this._cp = _cp;
        this._rue = _rue;
    }

    public int get_numeroRue() {
        return _numeroRue;
    }

    public void set_numeroRue(int _numeroRue) {
        this._numeroRue = _numeroRue;
    }

    public String get_type() {
        return _type;
    }

    public void set_type(String _type) {
        this._type = _type;
    }

    public String get_ville() {
        return _ville;
    }

    public void set_ville(String _ville) {
        this._ville = _ville;
    }

    public int get_cp() {
        return _cp;
    }

    public void set_cp(int _cp) {
        this._cp = _cp;
    }

    public String get_rue() {
        return _rue;
    }

    public void set_rue(String _rue) {
        this._rue = _rue;
    }

    public String getAdresseComplete() {
        return _numeroRue + " " +_type + " " + _rue + ", " + _cp + " " + _ville.toUpperCase();
    }
}
