package com.example.cdsm04.javaapp;

import android.util.Log;

public class Alignement {
    private String alignementLoi;
    private String alignementBien;


    public Alignement(String _alignementLoi, String _alignementBien) {
        this.setAlignementBien(_alignementBien);
        this.setAlignementLoi(_alignementLoi);
    }

    public String getAlignementLoi() {
        return alignementLoi;
    }

    public void setAlignementLoi(String _alignementLoi) {
        if (!_alignementLoi.equals("Loyal") || !_alignementLoi.equals("Neutre") || !_alignementLoi.equals("Chaotique"))
            Log.i("Test", "fzfzefzef");

        this.alignementLoi = _alignementLoi;
    }

    public String getAlignementBien() {
        return alignementBien;
    }

    public void setAlignementBien(String _alignementBien) {
        if (!_alignementBien.equals("Bon") || !_alignementBien.equals("Neutre") || !_alignementBien.equals("Mauvaus"))
            Log.i("Test", "fzfzefzef");

        this.alignementBien = _alignementBien;
    }

    public String getAlignementComplet() {
        return getAlignementLoi() + " " + getAlignementBien();
    }
}
