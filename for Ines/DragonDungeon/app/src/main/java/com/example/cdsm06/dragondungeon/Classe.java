package com.example.cdsm06.dragondungeon;

import android.util.Log;

public class Classe {

    public String TAG = "Test.Classe";

    private String mNomClasse;

    // Constructeur
    public Classe(String nomClasse) {

        this.setNomClasse(nomClasse);
    }

    // Getter Setter
    public String getNomClasse() {
        return mNomClasse;
    }

    public void setNomClasse(String nomClasse) {

        if (!mNomClasse.equals("Pretre")
                && !mNomClasse.equals("Guerrier")
                && !mNomClasse.equals("Voleur")
                && !mNomClasse.equals("Mage")
                )
//        {
//            Log.e(TAG, "setNomClasse: Nom de classe inconnu");
//        }
        mNomClasse = nomClasse;
    }
}
