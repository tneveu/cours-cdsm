package com.example.cdsm06.dragondungeon;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

public class FichePersoActivity extends AppCompatActivity {


    public String TAG = "Test.FichePerso";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fiche_perso);


        // Creation du personnage
        Personnage personnage = new Personnage("Dim", "KEK");
        Log.i(TAG, "onCreate: " + personnage.getNomJoueur() + " " + personnage.getNomPersonnage() + " " + personnage.getExpPersonnage());

        // Creation Race
        Race humain = new Race("Humain");
        Log.i(TAG, "onCreate: " + humain.getNomRace());

        // Creation Classe
        Classe guerrier = new Classe("Voleur");

        // Creation Alignement
        Alignement alignementPersonnage = new Alignement("Chaotique", "Neutre");

        // Assigner à 1 Perso une race et une classe
        personnage.setRacePersonnage(humain);
        personnage.setClassePersonnage(guerrier);
        personnage.setAlignementPersonnage(alignementPersonnage);
        personnage.mCaracteristiquesPersonnage.get("Force").setValeurCaracteristique(12);
        personnage.mCaracteristiquesPersonnage.get("Dexterite").setValeurCaracteristique(16);
        personnage.mCaracteristiquesPersonnage.get("Constitution").setValeurCaracteristique(12);
        personnage.mCaracteristiquesPersonnage.get("Intelligence").setValeurCaracteristique(12);
        personnage.mCaracteristiquesPersonnage.get("Sagesse").setValeurCaracteristique(8);
        personnage.mCaracteristiquesPersonnage.get("Charisme").setValeurCaracteristique(12);


        // TV = TextView pour mieux cibler le nom de variable
        final TextView tv_nomJoueur = (TextView) findViewById(R.id.activity_fiche_perso_nom_joueur_text);
        tv_nomJoueur.setText(personnage.getNomJoueur());

        final TextView tv_nomPersonnage = (TextView) findViewById(R.id.activity_fiche_perso_nom_personnage_text);
        tv_nomPersonnage.setText(personnage.getNomPersonnage());

        final TextView tv_racePersonnage = (TextView) findViewById(R.id.activity_fiche_perso_race_personnage_text);
        tv_racePersonnage.setText(personnage.getRacePersonnage().getNomRace());

        final TextView tv_classePersonnage = (TextView) findViewById(R.id.activity_fiche_perso_classe_personnage_text);
        tv_classePersonnage.setText(personnage.getClassePersonnage().getNomClasse());

        final TextView tv_alignementPersonnage = (TextView) findViewById(R.id.activity_fiche_perso_alignement_personnage_text);
        tv_alignementPersonnage.setText(personnage.getAlignementCompletPersonnage());

        // final TextView experiencePersonnage = (TextView) findViewById(R.id.activity_fiche_perso_alignement_personnage_text);


        // STATS

        final TextView tv_valeurForce = (TextView) findViewById(R.id.activity_fiche_perso_force_text);
        tv_valeurForce.setText(personnage.mCaracteristiquesPersonnage.get("Force").getValeurCaracteristique());
        final TextView tv_modificateurForce = (TextView) findViewById(R.id.activity_fiche_perso_force_text);
        tv_modificateurForce.setText(personnage.mCaracteristiquesPersonnage.get("Force").getModificateurCaracteristique());


        final TextView tv_valeurDexterite = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_valeurDexterite.setText(personnage.mCaracteristiquesPersonnage.get("Dexterite").getValeurCaracteristique());
        final TextView tv_modificateurDexterite = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_modificateurDexterite.setText(personnage.mCaracteristiquesPersonnage.get("Dexterite").getModificateurCaracteristique());


        final TextView tv_valeurConstitution = (TextView) findViewById(R.id.activity_fiche_perso_constitution_text);
        tv_valeurConstitution.setText(personnage.mCaracteristiquesPersonnage.get("Constitution").getValeurCaracteristique());
        final TextView tv_modificateurConstitution = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_modificateurConstitution.setText(personnage.mCaracteristiquesPersonnage.get("Constitution").getModificateurCaracteristique());


        final TextView tv_valeurIntelligence = (TextView) findViewById(R.id.activity_fiche_perso_intelligence_text);
        tv_valeurIntelligence.setText(personnage.mCaracteristiquesPersonnage.get("Intelligence").getValeurCaracteristique());
        final TextView tv_modificateurIntelligence = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_modificateurIntelligence.setText(personnage.mCaracteristiquesPersonnage.get("Intelligence").getModificateurCaracteristique());


        final TextView tv_valeurSagesse = (TextView) findViewById(R.id.activity_fiche_perso_sagesse_text);
        tv_valeurSagesse.setText(personnage.mCaracteristiquesPersonnage.get("Sagesse").getValeurCaracteristique());
        final TextView tv_modificateurSagesse = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_modificateurSagesse.setText(personnage.mCaracteristiquesPersonnage.get("Sagesse").getModificateurCaracteristique());


        final TextView tv_valeurCharisme = (TextView) findViewById(R.id.activity_fiche_perso_charisme_text);
        tv_valeurCharisme.setText(personnage.mCaracteristiquesPersonnage.get("Charisme").getValeurCaracteristique());
        final TextView tv_modificateurCharisme = (TextView) findViewById(R.id.activity_fiche_perso_dexterite_text);
        tv_modificateurCharisme.setText(personnage.mCaracteristiquesPersonnage.get("Charisme").getModificateurCaracteristique());

    }
}
