package com.example.cdsm06.dragondungeon;

import android.util.Log;

public class Caracteristique {

    public String TAG = "Test.Caracteristique";

    private String mNomCaracteristique;
    private int mValeurCaracteristique;

    // Constructeur
    public Caracteristique(String nomCaracteristique, int valeurCaracteristique) {
        this.setNomCaracteristique(nomCaracteristique);
        mValeurCaracteristique = valeurCaracteristique;
    }

    // Getter Setter
    public String getNomCaracteristique() {

        return mNomCaracteristique;
    }

    public void setNomCaracteristique(String nomCaracteristique) {

        if (!nomCaracteristique.equals("Force")
                && !nomCaracteristique.equals("Dexterite")
                && !nomCaracteristique.equals("Constitution")
                && !nomCaracteristique.equals("Intelligence")
                && !nomCaracteristique.equals("Sagesse")
                && !nomCaracteristique.equals("Charisme")
                )
//        {
//            Log.i(TAG, "setNomCaracteristique: caracteristique inconnue");
//        }

        mNomCaracteristique = nomCaracteristique;
    }

    public int getValeurCaracteristique() {

        return mValeurCaracteristique;
    }

    public void setValeurCaracteristique(int valeurCaracteristique) {

        mValeurCaracteristique = valeurCaracteristique;
    }

    public int getModificateurCaracteristique() {

        return (int) Math.floor((this.getValeurCaracteristique() - 10) / 2);
    }
}
