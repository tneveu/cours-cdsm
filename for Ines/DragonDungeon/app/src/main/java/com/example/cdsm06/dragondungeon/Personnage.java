package com.example.cdsm06.dragondungeon;

import java.util.HashMap;
import java.util.Map;

public class Personnage {

    private String mNomJoueur, mNomPersonnage;
    private int mExpPersonnage;
    private Race mRacePersonnage;
    private Classe mClassePersonnage;
    private Alignement mAlignementPersonnage;
    // Map - comme un tableau auquel on peut acceder par un nom au lieu de index classique
    public Map<String, Caracteristique> mCaracteristiquesPersonnage;

    // Constructeur

    public Personnage(String nomJoueur, String nomPersonnage) {
        mNomJoueur = nomJoueur;
        mNomPersonnage = nomPersonnage;
        mExpPersonnage = 0;


        // Façon Bruno
/*        mCaracteristiquesPersonnage = new HashMap() {
            {
                put("Force", new Caracteristique("Force", 0));
                put("Dexterite", new Caracteristique("Dexterite", 0));
                put("Constitution", new Caracteristique("Dexterite", 0));
                put("Intelligence", new Caracteristique("Intelligence", 0));
                put("Sagesse", new Caracteristique("Sagesse", 0));
                put("Charisme", new Caracteristique("Charisme", 0));
            }
        };*/

        // Initialise la liste des caracteristiques
        mCaracteristiquesPersonnage = new HashMap<>();

        Caracteristique force = new Caracteristique("Force",0);
        Caracteristique dexterite = new Caracteristique("Dexterite",0);
        Caracteristique constitution = new Caracteristique("Constitution",0);
        Caracteristique intelligence = new Caracteristique("Intelligence",0);
        Caracteristique sagesse = new Caracteristique("Sagesse",0);
        Caracteristique charisme = new Caracteristique("Charisme",0);

        mCaracteristiquesPersonnage.put("Force", force);
        mCaracteristiquesPersonnage.put("Dexterite", dexterite);
        mCaracteristiquesPersonnage.put("Constitution", constitution);
        mCaracteristiquesPersonnage.put("Intelligence", intelligence);
        mCaracteristiquesPersonnage.put("Sagesse", sagesse);
        mCaracteristiquesPersonnage.put("Charisme", charisme);
    }

    // Getter Setter
    public String getNomPersonnage() {
        return mNomPersonnage;
    }

    public void setNomPersonnage(String nomPersonnage) {
        mNomPersonnage = nomPersonnage;
    }

    public String getNomJoueur() {
        return mNomJoueur;
    }

    public void setNomJoueur(String nomJoueur) {
        mNomJoueur = nomJoueur;
    }

    public int getExpPersonnage() {
        return mExpPersonnage;
    }

    public void setExpPersonnage(int exp) {
        mExpPersonnage = exp;
    }

    public Race getRacePersonnage() {
        return mRacePersonnage;
    }

    public void setRacePersonnage(Race racePersonnage) {
        mRacePersonnage = racePersonnage;
    }

    public Classe getClassePersonnage() {
        return mClassePersonnage;
    }

    public void setClassePersonnage(Classe classePersonnage) {
        mClassePersonnage = classePersonnage;
    }

    public Alignement getAlignementPersonnage() {
        return mAlignementPersonnage;
    }

    public void setAlignementPersonnage(Alignement alignementPersonnage) {
        mAlignementPersonnage = alignementPersonnage;
    }

    public String getAlignementCompletPersonnage() {
        return mAlignementPersonnage.getAlignementComplet();
    }
}
