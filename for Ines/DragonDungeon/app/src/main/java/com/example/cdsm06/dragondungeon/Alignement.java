package com.example.cdsm06.dragondungeon;

import android.util.Log;

public class Alignement {

    // TAG pour le log
    private String TAG = "Test.Alignement";

    // Allignement Loi
    private String mAlignementLoi;
    // Allignement Bien
    private String mAlignementBien;


    // Constructeur
    public Alignement(String alignementLoi, String alignementBien) {

        this.setAlignementLoi(alignementLoi);
        this.setAlignementBien(alignementBien);
    }

    // Getter Setter
    public String getAlignementLoi() {
        return mAlignementLoi;
    }

    public void setAlignementLoi(String alignementLoi) {

        if (!mAlignementLoi.equals("Loyal")
                && !mAlignementLoi.equals("Neutre")
                && !mAlignementLoi.equals("Chaotique")
                )
//        {
//            Log.i(TAG, "setAlignementLoi: Alignement Inconnu");
//        }

        mAlignementLoi = alignementLoi;
    }

    public String getAlignementBien() {
        return mAlignementBien;
    }

    public void setAlignementBien(String alignementBien) {

        if (!mAlignementLoi.equals("Bon")
                && !mAlignementLoi.equals("Neutre")
                && !mAlignementLoi.equals("Mauvais")
                ) {
            Log.i(TAG, "setAlignementLoi: Alignement Inconnu");
        }

        mAlignementBien = alignementBien;
    }

    public String getAlignementComplet() {
        return getAlignementLoi() + " " + getAlignementBien();
    }
}
