package com.example.cdsm06.dragondungeon;

import android.util.Log;

public class Race {

    public String TAG = "Test.Race";

    private String mNomRace;

    // Constructeur
    public Race(String nomRace) {

        this.setNomRace(nomRace) ;
    }

    // Getter Setter
    public String getNomRace() {
        return mNomRace;
    }

    public void setNomRace(String nomRace) {

        if (!mNomRace.equals("Nain")
                && !mNomRace.equals("Elfe")
                && !mNomRace.equals("Halfling")
                && !mNomRace.equals("Humain")
                )
//        {
//            Log.e(TAG, "setNomRace: Nom de race inconnue");
//        }
            mNomRace = nomRace;
    }
}
